package com.vikas.ather_2048.game

import android.content.Context
import com.vikas.ather_2048.R

class GameScoreDataUtils(context: Context) {

    private val preferenceKey = context.getString(R.string.game_data_preference_key)
    private val highScoreKey = context.getString(R.string.game_data_highest_score_key)
    private val sPref = context.getSharedPreferences(this.preferenceKey, Context.MODE_PRIVATE)

    // Push highscore data to user defaults
    fun putHighScore(newHS : Int) {
        val editor = sPref.edit()
        editor.putInt(this.highScoreKey, newHS)
        editor.apply()
    }

    // Get highscore data from user defaults
    fun getHighScore() : Int {
        return this.sPref.getInt(this.highScoreKey, 0)
    }
}