package com.vikas.ather_2048.game

import java.util.*

class Game(listener: GameDataListener) : GameEngine(listener = listener) {

    private var startedPlaying : Date = Date()
    private var startLastGame : Date

    init {
        this.startLastGame = this.startedPlaying
    }

    override fun newGame(newHighScore: Int) {
        super.newGame(newHighScore)
        this.startLastGame = Date()
    }
}