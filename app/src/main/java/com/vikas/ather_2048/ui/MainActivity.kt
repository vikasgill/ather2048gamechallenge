package com.vikas.ather_2048

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.MotionEvent
import android.view.View
import android.widget.GridLayout
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.vikas.ather_2048.game.*
import com.vikas.ather_2048.util.Constants


class MainActivity : AppCompatActivity(), GameDataListener {

    private var x1 = 0f
    private var y1 = 0f
    private lateinit var game: Game
    private lateinit var dataStore : GameScoreDataUtils
    private var highScore : Int = 0
    private var currentScore:Int = 0
    private val cells = IntArray(16)
    private lateinit var tvHighestScore:TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.game_view)
        dataStore = GameScoreDataUtils(this.baseContext)

        cells[0] = R.id.index0
        cells[1] = R.id.index1
        cells[2] = R.id.index2
        cells[3] = R.id.index3
        cells[4] = R.id.index4
        cells[5] = R.id.index5
        cells[6] = R.id.index6
        cells[7] = R.id.index7
        cells[8] = R.id.index8
        cells[9] = R.id.index9
        cells[10] = R.id.index10
        cells[11] = R.id.index11
        cells[12] = R.id.index12
        cells[13] = R.id.index13
        cells[14] = R.id.index14
        cells[15] = R.id.index15

        tvHighestScore = findViewById(R.id.highest_score)
        var maxScore = dataStore.getHighScore()
        tvHighestScore.text = StringBuffer(Constants.HIGHEST_SCORE_TITLE).append(maxScore)

        game = Game(this)
        this.setupNewGame()
    }

    // Reset game
    private fun setupNewGame() {
        this.highScore = dataStore.getHighScore()
        game.newGame(this.highScore)
        this.userScoreChanged(0)
    }

    /**
     * To handle the user touch events:
     * Swipe events : Left, Right, Up, Down
     */
    override fun onTouchEvent(event: MotionEvent): Boolean {

        when (event.action) {
            // when user first touches the screen we get x and y coordinate
            MotionEvent.ACTION_DOWN -> {
                x1 = event.x
                y1 = event.y
            }
            MotionEvent.ACTION_UP -> {
                val x2 = event.x
                val y2 = event.y
                val minDistance = 200

                if (x1 < x2 && x2 - x1 > minDistance) { game.actionMove(Direction.Right) }
                else if (x1 > x2 && x1 - x2 > minDistance) { game.actionMove(Direction.Left) }
                else if (y1 < y2 && y2 - y1 > minDistance) { game.actionMove(Direction.Down) }
                else if (y1 > y2 && y1 - y2 > minDistance) { game.actionMove(Direction.Up) }
            }
        }
        return super.onTouchEvent(event)
    }

    private fun paintTransition(move: GameEngine.Transition) {

        val tv = findViewById<TextView>(cells[move.location])

        if (move.action == TileOperation.Slide || move.action == TileOperation.Merge) {
            paintCell(tv, move.value)
            this.paintTransition(GameEngine.Transition(TileOperation.Clear, 0, move.oldLocation))
        } else {
            paintCell(tv, move.value)
        }
    }

    private fun paintCell(obj: Any, value: Int) {

        val tvCell = obj as TextView
        tvCell.text = if (value <= 0) "" else "$value"

        var txCol = resources.getColor(R.color.t_dark_text, null)
        var bgCol = resources.getColor(R.color.t0_bg, null)

        when (value) {
            2 -> bgCol = resources.getColor(R.color.t2_bg, null)
            4 -> bgCol = resources.getColor(R.color.t4_bg, null)
            8 -> {
                txCol = resources.getColor(R.color.t_light_text, null)
                bgCol = resources.getColor(R.color.t8_bg, null)
            }
            16 -> {
                txCol = resources.getColor(R.color.t_light_text, null)
                bgCol = resources.getColor(R.color.t16_bg, null)
            }
            32 -> {
                txCol = resources.getColor(R.color.t_light_text, null)
                bgCol = resources.getColor(R.color.t32_bg, null)
            }
            64 -> {
                txCol = resources.getColor(R.color.t_light_text, null)
                bgCol = resources.getColor(R.color.t64_bg, null)
            }
            128 -> {
                txCol = resources.getColor(R.color.t_light_text, null)
                bgCol = resources.getColor(R.color.t128_bg, null)
            }
            256 -> bgCol = resources.getColor(R.color.t256_bg, null)
            512 -> bgCol = resources.getColor(R.color.t512_bg, null)
            1024 -> bgCol = resources.getColor(R.color.t1024_bg, null)
            2048 -> bgCol = resources.getColor(R.color.t2048_bg, null)
            else -> { } // won't happen
        }
        tvCell.setBackgroundColor(bgCol)
        tvCell.setTextColor(txCol)
    }

    override fun userWon() {
        if(this.currentScore > this.dataStore.getHighScore()) {
            this.highScore = currentScore
            this.dataStore.putHighScore(this.currentScore)
        }

        var toast = Toast.makeText(this, resources.getString(R.string.win_toast_message) +
                game.maxTile, Toast.LENGTH_LONG)

        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()

    }

    override fun userLose() {
        if(this.currentScore > this.dataStore.getHighScore()) {
            this.highScore = currentScore
            this.dataStore.putHighScore(this.currentScore)
        }

        var toast = Toast.makeText(this, resources.getString(R.string.lose_toast_message),
            Toast.LENGTH_LONG)
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()
    }

    override fun userHighestScore(score: Int) {
        this.dataStore.putHighScore(score)
        this.highScore = score

        var toast = Toast.makeText(this, resources.getString(R.string.user_highest_score_toast_message),
            Toast.LENGTH_SHORT)
        toast.setGravity(Gravity.CENTER, 0, 0)
        toast.show()

    }

    override fun userScoreChanged(score: Int) {
        this.currentScore = score
        val tv: TextView = findViewById(R.id.score)
        tv.text = StringBuffer(Constants.CURRENT_SCORE_TITLE).append(score)
    }

    override fun updateTileValue(move: GameEngine.Transition) {
        paintTransition(move)
    }

    fun newGame(view: View) {
        game.newGame(this.dataStore.getHighScore())
    }

    fun undoMove(view: View) {
        game.goBackOneMove()
    }

    override fun onBackPressed() {
        createExitTipDialog()
    }

    private fun createExitTipDialog() {
        AlertDialog.Builder(this@MainActivity)
            .setMessage("Are you sure to exit？")
            .setTitle("Confirm")
            .setPositiveButton("Yes") { dialogInterface, _ ->
                dialogInterface.dismiss()
                finish()
            }
            .setNegativeButton("No"
            ) { dialogInterface, _ -> dialogInterface.dismiss() }
            .show()
    }

}